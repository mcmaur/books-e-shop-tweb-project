<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<%@page import="myPackage.Libro" %>
<%@ include file="../common/function_jsp"%>

<!DOCTYPE html>
<html>
<head>
<script>
	function verifica(modulo)
	{
		if (modulo.titolo.value == "" && modulo.autore.value == "")
		{			
			alert("Titolo e autore field blank.\nPlease fill in");
			return false;
		}
		return true;
	}
	
	function del(){
		return confirm("Sei sicuro di voler eliminare l'elemento?");
	}
</script>
<link rel="stylesheet" type="text/css" media="screen"
	href="./style/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Pagina di ricerca</title>
</head>
<body>
	<%@ include file="../common/header.txt"%>
	<%@ include file="../common/nav.txt"%>
	<%@ include file="../common/sx_coloumn" %>
	
	<article>
		<div id="formcerca" style="text-align:center;">
			Cerca i libri
			<form action="Controller" onSubmit="return verifica(this);"
				method="post">
				Titolo: <input type="text" id="titolo" name="titolo" /><br /> Autore:
				<input type="text" id="autore" name="autore" /><br /> 
				<input type="hidden" name="type" value="cerca" /> 
				<input type="submit" name="submit" value="OK" />
			</form>
		</div>
		
		<%
		//HttpSession sess = request.getSession();
		if(session.getAttribute("level").equals("administrator"))
		{
			out.print("<form action=\"Controller\" method=\"post\">" +
					"<input type=\"hidden\" name=\"type\" value=\"add\">" +
					"<input type=\"submit\" name=\"aggiungi\" value=\"aggiungi libro\"/>"+
					"</form>");
		}
		
		if(request.getAttribute("tabella") != null)
		{
			@SuppressWarnings("unchecked")
			Vector <Libro> lbvector = (Vector <Libro>) request.getAttribute("tabella");
			out.println("</br></br>");
			out.print("<table class=\"tabelle\" ><tr><th>Nome</th><th>Titolo</th><th>ISBN</th><th>Prezzo</th><th>Copie disponibili</th>");
			if(!(session.getAttribute("level").equals("none")))
				out.print("<th>Quantita'</th>");
			if((session.getAttribute("level").equals("administrator")))
				out.print("<th>Modifica</th><th>Elimina</th></tr>");
			for(int index=0;index<lbvector.size();index++)
			{
				Libro lbtmp=lbvector.elementAt(index);
				
				out.print("<tr " + (index%2==0?"":"class=\"riga_colorata\"") + ">");
				
				
				out.print("<td>"+lbtmp.getAutore()+"</td>");
				out.print("<td>"+lbtmp.getTitolo()+"</td>");
				out.print("<td>"+lbtmp.getIsbn()+"</td>");
				out.print("<td>"+lbtmp.getPrezzo()+"</td>");
				out.print("<td>"+lbtmp.getCopie()+"</td>");

				if(!(session.getAttribute("level").equals("none")))
				{
					String dis="";
					out.println("<td><form action=\"Controller\" method=\"post\">" +
							"<select name=\"numcopie\" id=\"numcopie\">");
					for(int i=1;i<(lbtmp.getCopie()+1);i++)
						out.println("<option>"+i+"</option>");
					if(lbtmp.getCopie()==0)
						dis="disabled";
					
					out.println("</select>"+
							"<input type=\"hidden\" name=\"type\" value=\"prenota_ISBN\">" +
							"<input type=\"hidden\" name=\"numero_copie\" value=\"1\">" +							
							"<input type=\"hidden\" name=\"ISBN\" value=\"" + lbtmp.getIsbn() + "\">" +
										"<input type=\"submit\" name=\"prenota\" value=\"prenota\"" + dis + "/></form></td>");
				}
				if(session.getAttribute("level").equals("administrator"))
				{
					out.println("<td><form action=\"Controller\" method=\"post\">" +
							"<input type=\"hidden\" name=\"type\" value=\"modifica_ISBN\">" +
							"<input type=\"hidden\" name=\"ISBN\" value=\"" + lbtmp.getIsbn() + "\">" +
							"<input type=\"submit\" name=\"modifica\" value=\"modifica\"/></form></td>");
					out.println("<td><form action=\"Controller\" method=\"post\" onSubmit=\"return del()\">" +
							"<input type=\"hidden\" name=\"type\" value=\"elimina_ISBN\">" +
							"<input type=\"hidden\" name=\"ISBN\" value=\"" + lbtmp.getIsbn() + "\">" +
							"<input type=\"submit\" name=\"elimina\" value=\"elimina\"/></form></td>");
				}
				out.println("</tr>");
			}
			out.println("</table>");
			request.removeAttribute("tabella");
		}
	%>
	</article>

	<%@ include file="../common/dx_coloumn" %>
	<%@ include file="../common/footer.txt"%>
</body>
</html>