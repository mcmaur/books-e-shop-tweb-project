<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../common/function_jsp"%>
<!DOCTYPE html >
<html>
<head>
<link rel="stylesheet" type="text/css" media="screen"
	href="./style/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>contatti</title>
</head>
<body>
	<%@ include file="../common/header.txt"%>
	<%@ include file="../common/nav.txt"%>
	<%@ include file="../common/sx_coloumn" %>

	<article>
		<div style="text-align:center;">
			Per contattare il nostro servizio clienti<br /> <br /> Telefono: <br />
			011-8190384816 <br /> Email: <br /> libreria-online@libs.it <br />
			Fax: <br /> 011-8190384817
		</div>
	</article>

	<%@ include file="../common/dx_coloumn" %>
	<%@ include file="../common/footer.txt"%>
</body>
</html>