<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../common/function_jsp"%>
<!DOCTYPE html>
<html>
<head>
<script>
function initialize() 
{
var latlng = new google.maps.LatLng(45.090035,7.659196);
var myOptions = 
{
	zoom: 15,
	center: latlng,
	mapTypeId: google.maps.MapTypeId.HYBRID,
	mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR}
}
	mymap = new google.maps.Map(document.getElementById("map"), myOptions);
	var marker = new google.maps.Marker({
	position: latlng,
	map: mymap,
	title:"Ci troviamo qui"
	});
}
</script>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<script type="text/javascript"
	src="http://maps.google.com/maps/api/js?sensor=true&language=it"></script>

<link rel="stylesheet" type="text/css" media="screen"
	href="./style/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Dove siamo</title>
</head>
<body onload="initialize()">
	<%@ include file="../../common/header.txt"%>
	<%@ include file="../../common/nav.txt"%>
	<%@ include file="../../common/sx_coloumn" %>

	<article>
		SEDE<br /> <br /> La nostra sede si trova presso il centro "Centro
		Direzionale Piero della Francesca"<br /> in Corso Svizzera 185/bis –
		10149 Torino – Italy<br /> <br />
		<div id="map"><noscript>Your browser does not support Javascript!</noscript></div>
		<br />Link: <br /> <a href="http://www.centropdf.it">centropdf</a>
	</article>
	
	<%@ include file="../../common/dx_coloumn" %>
	<%@ include file="../../common/footer.txt"%>
</body>
</html>