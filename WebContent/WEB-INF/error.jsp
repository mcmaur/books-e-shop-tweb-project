<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../common/function_jsp"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" media="screen" href="./style/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>error page</title>
</head>
<body>
	<%@ include file="../common/header.txt"%>
	<%@ include file="../common/nav.txt"%>
	<%@ include file="../common/sx_coloumn" %>

	<article>
		<div id="error" style="text-align:center; border: 1px solid red; font-size:120%;">
			<%= request.getAttribute("result") %>
			<% request.removeAttribute("result"); %>
		</div>
		<div id="photoerror" style="position:absolute; margin-left:20%; margin-right:30%; border: 1px solid black;">
			<img src="images/error.jpg" width=60%>	
		</div>
	</article>
	
	<%@ include file="../common/dx_coloumn" %>
	<%@ include file="../common/footer.txt"%>
</body>
</html>