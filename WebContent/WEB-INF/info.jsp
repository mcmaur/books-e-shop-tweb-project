<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../common/function_jsp"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" media="screen"
	href="./style/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Info azienda</title>
</head>
<body>
	<%@ include file="../common/header.txt"%>
	<%@ include file="../common/nav.txt"%>
	<%@ include file="../common/sx_coloumn" %>

	<article>

		<br /> COSTITUZIONE <br /> <br /> Siao una societ&agrave nata nel 2012
		nata dall'idea di tre giovani studenti universitari pronti
		all'innovazione ma altrettanto carichi di esperienza per guidare una
		grande azienda come la nostra. <br /> <br /> ESPERIENZA E
		PROFESSIONALITÀ <br /> <br /> La Societ&agrave opera e collabora con
		grandi risultati nell ambito dell'editoria. <br /> Con sede nel
		"Centro Piero della Francesca" , Torino, esercita su tutto il
		territorio nazionale.<br /> <br /> La serietà e la professionalità di
		questa Ditta, la cura nel dettaglio, l' utilizzo di strutture
		adeguate, e l'attenzione alle esigenze del cliente, le consentono
		sempre il raggiungimento degli obbiettivi imposti.

	</article>

	<%@ include file="../common/dx_coloumn" %>
	<%@ include file="../common/footer.txt"%>
</body>
</html>