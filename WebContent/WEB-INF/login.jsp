<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../common/function_jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Pagina di login</title>
<link rel="stylesheet" type="text/css" media="screen"
	href="./style/style.css" />
<script>
	function verifica(modulo)
	{
		if (modulo.login.value == "")
		{			
			alert("Username field blank.\nPlease fill in");
			modulo.login.focus();
			return false;
		}
		if(modulo.pwd.value == "")
		{			
			alert("Password field blank.\nPlease fill in");
			modulo.pwd.focus();
			return false;
		}
		return true;
	}
	</script>
</head>
<body>
	<%@ include file="../common/header.txt"%>
	<%@ include file="../common/nav.txt"%>
	<%@ include file="../common/sx_coloumn" %>

	
	
		<div id="formLogin" style="text-align:center;">
			<form action="Controller" onSubmit="return verifica(this);"
				method="post">
				Username: <input type="text" id="login" name="login" /><br />
				Password: <input type="password" id="pwd" name="pwd" /><br /> <input
					type="hidden" name="type" value="login" /> <input type="submit"
					name="submit" value="OK" />
			</form>

			Sei nuovo del sito? Registrati <a href="register.jsp">qui</a>
		</div>
		
	
	<%@ include file="../common/dx_coloumn" %>
	<%@ include file="../common/footer.txt" %>
</body>
</html>