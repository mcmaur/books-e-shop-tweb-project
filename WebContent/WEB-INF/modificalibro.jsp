<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@page import="myPackage.Libro" %>
<%@ include file="../common/function_jsp"%>
<!DOCTYPE html>
<html>
<head>
<script>
function verifica(modulo)
{
	if(documento.modulo.titolo.value=="" || documento.modulo.autore.value=="" || documento.modulo.prezzo.value=="" || documento.modulo.copie.value=="" )
	{
		alert("Attenzione! Alcuni campi sono vuoti");
		return false;
	}
	if(documento.modulo.prezzo.value<0 || documento.modulo.copie.value<0)
	{
		alert("Attenzione! Campi negativi presenti");
		return false;
	}
	if(!isNumber(documento.modulo.prezzo.value) || !isNumber(documento.modulo.copie.value))
	{
		alert("Attenzione! I campi prezzo e Copie devono essere numerici!!!");
		return false;
	}
	return true;
}


function isNumber(value) {
    if ((undefined === value) || (null === value)) {
        return false;
    }
    if (typeof value == 'number') {
        return true;
    }
    return !isNaN(value - 0);
}

</script>
<link rel="stylesheet" type="text/css" media="screen" href="./style/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Modifica Libro</title>
</head>
<body>
	<%@ include file="../common/header.txt"%>
	<%@ include file="../common/nav.txt"%>
	<%@ include file="../common/sx_coloumn" %>

	<article>
	<%
		if(request.getAttribute("formModificaLibro") != null)
		{
			Libro lbr = (Libro) request.getAttribute("formModificaLibro");
			String block= (String)request.getAttribute("add");
			
			out.println("<div style=\"text-align:center;\">");
				out.println("<form action=\"Controller\" onSubmit=\"return verifica(this);\"method=\"post\">");
				out.println("TITOLO: <input type=\"text\" id=\"titolo\" name=\"titolo\"value=\""+lbr.getTitolo()+"\"/>"+"<br/>");
				out.println("AUTORE: <input type=\"text\" id=\"autore\" name=\"autore\"value=\""+lbr.getAutore()+"\"/>"+"<br/>");
				out.println("ISBN: <input type=\"textarea\" id=\"isbn_bck\" name=\"isbn_bck\" value=\""+lbr.getIsbn()+"\"" + block + ">"+"<br/>");
				out.println("PREZZO: <input type=\"text\" id=\"prezzo\" name=\"prezzo\"value=\""+lbr.getPrezzo()+"\"/>"+"<br/>");
				out.println("COPIE: <input type=\"text\" id=\"copie\" name=\"copie\"value=\""+lbr.getCopie()+"\"/>"+"<br/>");
				out.println("<input type=\"hidden\" id=\"type\" name=\"type\" value=\"modificaLibroAdmin\"/>");
				out.println("<input type=\"hidden\" id=\"isbn\" name=\"isbn\"value=\""+lbr.getIsbn()+"\"/>"+"<br/>");				
				out.println("<input type=\"submit\" id=\"submit\" name=\"submit\" value=\"submit\"/>");
				out.println("</form>");
			out.println("</div>");
		}
		request.removeAttribute("formModificaLibro");
	%>
	</article>

	<%@ include file="../common/dx_coloumn" %>
	<%@ include file="../common/footer.txt"%>
</body>
</html>