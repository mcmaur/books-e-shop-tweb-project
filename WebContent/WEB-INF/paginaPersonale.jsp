<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" import="java.util.*"%>
<%@page import="myPackage.Libro" %>
<%@ include file="../common/function_jsp"%>
<!DOCTYPE html>
<html>
<head>
<script>

</script>
<link rel="stylesheet" type="text/css" media="screen"
	href="./style/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Personal Page</title>
</head>
<body>
	<%@ include file="../common/header.txt"%>
	<%@ include file="../common/nav.txt"%>
	<%@ include file="../common/sx_coloumn" %>

	<article>
		<% 
		if(request.getAttribute("libriInPrestito") != null)
		{
			@SuppressWarnings("unchecked")
			Vector <Libro> lbvector = (Vector <Libro>) request.getAttribute("libriInPrestito");
			out.print("</br></br><div style=\"text-align:center;\">I tuoi Libri</div></br></br> ");
			out.print("<table class=\"tabelle\"><tr><th>Autore</th><th>Titolo</th><th>ISBN</th><th>Prezzo</th><th>Copie</th></tr>");
			for(int index=0;index<lbvector.size();index++)
			{
				Libro lbtmp=lbvector.get(index);
				out.print("<tr><td>"+lbtmp.getAutore()+"</td>");
				out.print("<td>"+lbtmp.getTitolo()+"</td>");
				out.print("<td>"+lbtmp.getIsbn()+"</td>");
				out.print("<td>"+lbtmp.getPrezzo()+"</td>");
				out.print("<td>"+lbtmp.getCopie()+"</td>");
				out.print("<td><form action=\"Controller\" method=\"post\">" +
							"<input type=\"hidden\" name=\"type\" value=\"restituisci_ISBN\">" +
						"<input type=\"hidden\" name=\"ISBN\" value=\"" + lbtmp.getIsbn() + "\">" +
						"<input type=\"submit\" name=\"restituisci\" value=\"annulla\"/></form></td>");
			}
			out.println("</tr>");
		}
		out.println("</table></br></br>");
		request.removeAttribute("libriInPrestito"); 
		
		out.print("<div style=\"text-align:right;\"><form action=\"Controller\" method=\"post\">" +
				"<input type=\"hidden\" name=\"type\" value=\"confirm\">" +
				"<input type=\"submit\" name=\"confirm\" value=\"Conferma acquisto\"/>"+
				"</form></div>");
		%>
		
		<%
		session = request.getSession();
		if(((String) session.getAttribute("level")).equals("administrator")){
			out.print("</br></br><AMMINISTRAZIONE> Gestione Prenotazioni</br></br> ");
			if(request.getAttribute("prenotazioni") != null)
			{
				@SuppressWarnings("unchecked")
				Vector <Libro> lbvector = (Vector <Libro>) request.getAttribute("prenotazioni");
				out.print("<table class=\"tabelle\"><tr><th>Nome</th><th>Autore</th><th>Titolo</th><th>ISBN</th></tr>");
				for(int index=0;index<lbvector.size();index++)
				{
					Libro lbtmp=lbvector.elementAt(index);
					out.print("<tr " + (index%2==0?"":"class=\"riga_colorata\"") + ">");
					out.print("<td>"+lbtmp.getExt()+"</td>");
					out.print("<td>"+lbtmp.getAutore()+"</td>");
					out.print("<td>"+lbtmp.getTitolo()+"</td>");
					out.print("<td>"+lbtmp.getIsbn()+"</td>");
					out.println("</tr>");
				}
				out.println("</table>");
				
			}
			request.removeAttribute("prenotazioni");
		}
		
		%>
		
	</article>

	<%@ include file="../../common/dx_coloumn" %>
	<%@ include file="../../common/footer.txt"%>
</body>
</html>