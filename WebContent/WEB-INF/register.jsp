<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../common/function_jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Pagina di registrazione</title>
<link rel="stylesheet" type="text/css" media="screen"
	href="./style/style.css" />
<script>
	function verifica(modulo)
	{
		if (modulo.login.value == "")
		{			
			alert("Username field blank.\nPlease fill in");
			modulo.login.focus();
			return false;
		}
		if(modulo.pwd.value == "")
		{			
			alert("Password field blank.\nPlease fill in");
			modulo.pwd.focus();
			return false;
		}
		if(modulo.email.value == "")
		{			
			alert("Email field blank.\nPlease fill in");
			modulo.email.focus();
			return false;
		}
		return true;
	}
	</script>
</head>
<body>
	<%@ include file="../common/header.txt"%>
	<%@ include file="../common/nav.txt"%>
	<%@ include file="../common/sx_coloumn" %>


	<article>
		<div id=formRegister style="text-align:center;">
			<form action="Controller" onSubmit="return verifica(this);"
				method="post">
				Username: <input type="text" id="login" name="login" /><br />
				Password: <input type="password" id="pwd" name="pwd" /><br /> Email:
				<input type="email" id="email" name="email" /><br /> <input
					type="hidden" value="register" /> <input type="submit"
					name="submit" value="OK" />
			</form>
			Sei già iscritto al sito? Loggati <a href="login.jsp">qui</a>
		</div>
	</article>

	<%@ include file="../common/dx_coloumn" %>
	<%@ include file="../common/footer.txt"%>
</body>
</html>