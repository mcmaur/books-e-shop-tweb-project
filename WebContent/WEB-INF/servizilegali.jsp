<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="../common/function_jsp"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" media="screen" href="./style/style.css" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Servizi legali</title>
</head>
<body>
	<%@ include file="../common/header.txt"%>
	<%@ include file="../common/nav.txt"%>
	<%@ include file="../common/sx_coloumn" %>
<article>
	<h3>Garanzia legale</h3><br/>
	Ogni prodotto venduto a un acquirente "consumatore" è assistito dalla garanzia legale sui beni di consumo, che 
	copre i difetti di conformità che si manifestano entro 24 mesi dalla data di consegna del bene.
	<br/><br/>
	<h5>Cos'è?</h5>
	È la garanzia sulla vendita di beni di consumo prevista dalla Direttiva comunitaria 1999/44/CE, detta anche
	"garanzia di conformità", che ti tutela nel caso in cui i prodotti che acquisti presentino difetti di conformità
	rispetto al contratto di vendita ("Garanzia Legale"). Ciò avviene, ad esempio, nel caso di prodotti che 
	funzionano male, che non possono essere utilizzati secondo le modalità dichiarate dal venditore o per le 
	finalità per cui vengono generalmente usati prodotti dello stesso tipo, che non hanno le caratteristiche o 
	qualità promesse dal venditore o che non rispondono all'uso per cui li hai acquistati.
	<br/><br/>
	Per ricevere assistenza in garanzia sui prodotti venduti puoi rivolgerti al Servizio Clienti cliccando
	 <a href="contatti.jsp">qui</a>.
	<h5>Durata</h5>
	La Garanzia Legale copre i difetti di conformità che si manifestano entro 24 mesi dalla data di consegna 
	del prodotto che hai acquistato, anche se a quella data il difetto non era immediatamente riscontrabile. Per 
	farla valere devi comunicare al venditore la presenza del difetto entro 2 mesi da quando lo hai scoperto.
	Ti consigliamo di conservare sempre la ricevuta di acquisto che trovi nella confezione del prodotto, nonché
	i documenti di conferma della spedizione e di consegna del prodotto.
	
	<h5>Diritti della Garanzia Legale</h5>
	
	Se il prodotto che hai acquistato presenta un difetto coperto dalla Garanzia Legale, hai diritto, senza che tu
	debba sostenere alcuna spesa, alla riparazione o sostituzione del prodotto difettoso.Hai diritto alla riduzione
	del prezzo oppure alla risoluzione del contratto se la sostituzione o la riparazione del prodotto non sono 
	possibili o sono eccessivamente onerose oppure se il venditore non ha provveduto né alla riparazione né alla
	sostituzione del prodotto in un periodo di tempo congruo oppure ancora se la sostituzione o la riparazione del 
	prodotto ti ha causato notevoli inconvenienti. Per determinare l'importo della riduzione del prezzo o la somma 
	a cui hai diritto in questi casi si tiene conto dell'uso che hai fatto del prodotto. Tieni presente che un 
	difetto di lieve entità per il quale non è stato possibile o è eccessivamente oneroso esperire i rimedi della
	riparazione o della sostituzione non dà diritto alla risoluzione del contratto.
	Cosa fare in presenza di un difetto di conformità?
	<br/><br/>
	Per contattare il nostro servizio legale:<br/>
	email: libreria-online@libs.it <br />
</article>
	<%@ include file="../common/dx_coloumn" %>
	<%@ include file="../common/footer.txt"%>
</body>
</html>