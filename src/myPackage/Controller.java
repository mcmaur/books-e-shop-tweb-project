package myPackage;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.*;

@WebServlet("/Controller")
public class Controller extends HttpServlet 
{
	private static final long serialVersionUID = 1L;
	private OPBean bean;
	
	@Override
	public void init(ServletConfig config) throws ServletException 
	{
		super.init(config);
		bean = new OPBean();
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		processRequest(request,response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		processRequest(request,response);
	}
	
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{	
		HttpSession session = request.getSession(); // creo la sessione utente o richiamo quella già creata
		if (session.getAttribute("login") == null) 
		{
			session.setAttribute("login", "anonymous");
			session.setAttribute("level", "none");
		}
		
		if(request.getParameter("type")==null)
		{
															/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
			RequestDispatcher rdErr = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
			request.setAttribute("result", "Non si può accedere direttamente al controller");
			rdErr.forward(request,response);
		}
		 if(request.getParameter("type").equals("login"))
		 gestisciLogin(request,response);
		 else if(request.getParameter("type").equals("register"))
		 gestisciRegister(request,response);
		 else if(request.getParameter("type").equals("cerca"))
			 gestisciRicerca(request,response);
		 else if(request.getParameter("type").equals("logout"))
			 gestisciLogout(request,response);
		 else if(request.getParameter("type").equals("restituisci_ISBN"))
			 restituisciLibro(request,response);
		 else if(request.getParameter("type").equals("prenota_ISBN"))
			 prenotaLibro(request,response);
		 else if(request.getParameter("type").equals("modifica_ISBN"))
			 modificaLibro(request,response);
		 else if(request.getParameter("type").equals("elimina_ISBN"))
			 eliminaLibro(request,response);
		 else if(request.getParameter("type").equals("libriinprestito"))
			 libriInPrestito(request,response);
		 else if(request.getParameter("type").equals("search"))
			 cercaLibri(request,response);
		 else if(request.getParameter("type").equals("add"))
			 addLibro(request,response);
		 else if(request.getParameter("type").equals("modificaLibroAdmin"))
			 modificaLibroAdmin(request,response);
		 else if(request.getParameter("type").equals("redirect"))
			 redirect(request,response);
		 else if(request.getParameter("type").equals("confirm"))
			 confermaAcquisto(request,response);
		 else
		 {
			 RequestDispatcher rdErr = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
			 request.setAttribute("result", "Errore scelta non possibile. Si prega di riprovare");
			 rdErr.forward(request,response);
		 }
	}
	
	protected void redirect(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		RequestDispatcher rd0 = getServletContext().getRequestDispatcher("/WEB-INF/"+request.getParameter("where"));
		rd0.forward(request,response);
	}
	
	protected void gestisciLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		RequestDispatcher rdERR = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
		RequestDispatcher rd0 = getServletContext().getRequestDispatcher("/WEB-INF/paginaPersonale.jsp");
		HttpSession s = request.getSession();
		String userName = request.getParameter("login");
		String password = request.getParameter("pwd");
		if(userName==null || password==null)
		{
			String tmp ="Login e/o Password non valide!!!";
			s.setAttribute("result", tmp);
			rdERR.forward(request,response);	
		}
		String risultato = bean.getLogin(userName,password);
		if(!risultato.equals("-1"))
		{
			s.setAttribute("login", userName);
			s.setAttribute("level", risultato);
			Vector <Libro> lbv= bean.libriInPrestito(userName);
			request.setAttribute("libriInPrestito", lbv);
			if(risultato.equals("administrator"))
			{
				Vector <Libro> ret = bean.getPrenotazioni();
				request.setAttribute("prenotazioni", ret);
			}
			else request.setAttribute("prenotazioni", null);
			rd0.forward(request,response);
		}
		else
		{
			request.setAttribute("result", "Login e/o Password non valide!!!");
			rdERR.forward(request,response);
		}
	}
	
	protected void libriInPrestito(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		HttpSession s = request.getSession();
		RequestDispatcher rdERR = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
		RequestDispatcher rd0 = getServletContext().getRequestDispatcher("/WEB-INF/paginaPersonale.jsp");
		if(s.getAttribute("login")!=null && !((String)s.getAttribute("login")).equals("anonymous") )
		{
			Vector <Libro> ret= bean.libriInPrestito((String)s.getAttribute("login"));
			request.setAttribute("libriInPrestito", ret);
			if(((String)s.getAttribute("level")).equals("administrator"))
			{
				ret=bean.getPrenotazioni();
				request.setAttribute("prenotazioni", ret);
			}
			else request.setAttribute("prenotazioni", null);
			request.setAttribute("level", (String)s.getAttribute("level"));
			rd0.forward(request,response);
		}
		else
		{
			request.setAttribute("result", "Non puoi vedere i tuoi libri perche' non sei loggato");
			rdERR.forward(request,response);
		}
			
	}
	
	protected void gestisciRegister(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		//eventualmente controllare campi nulli
		RequestDispatcher rdErr = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
		HttpSession s = request.getSession();
		PrintWriter out = response.getWriter();
		String userName = request.getParameter("login");
		String password = request.getParameter("pwd");
		String email = request.getParameter("email");
		if(bean.register(userName,password,email))
		{
			out.println("Registrazione andata bene, " + userName);
			//redirect home o login!!!!!!!!!!
		}
		else
		{
			String tmp ="Impossibile registrare l'utente!!!";
			s.setAttribute("result", tmp);
			rdErr.forward(request,response);
		}
	}
	
	protected void gestisciRicerca(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		HttpSession s = request.getSession();
		RequestDispatcher rdErr = getServletContext().getRequestDispatcher("/WEB-INF/errore.jsp");
		RequestDispatcher rd0 = getServletContext().getRequestDispatcher("/WEB-INF/cerca.jsp");
		String titolo = request.getParameter("titolo");
		String autore = request.getParameter("autore");
		if(titolo==null && autore==null)
		{
			request.setAttribute("result", "Autore e titolo mancanti!!!");
			rdErr.forward(request,response);
		}
		Vector <Libro> vecLib = bean.searchBook(titolo,autore,(String)s.getAttribute("level"));
		request.setAttribute("tabella", vecLib);
		rd0.forward(request,response);
	}
	
	protected void gestisciLogout(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		HttpSession s = request.getSession();
		s.setAttribute("login",null);
		s.setAttribute("level",null);
		s.invalidate();
		RequestDispatcher rd0 = getServletContext().getRequestDispatcher("/index.jsp");
		rd0.forward(request,response);
	}
	
	protected void restituisciLibro(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		RequestDispatcher rdErr = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
		RequestDispatcher rd0 = getServletContext().getRequestDispatcher("/WEB-INF/paginaPersonale.jsp");
		HttpSession s = request.getSession();
		if(s.getAttribute("login")!=null && !s.getAttribute("login").equals("anonymous"))
		{
			if(bean.restituisciLibro((String)s.getAttribute("login"), request.getParameter("ISBN")))
			{
				Vector <Libro> lbv = bean.libriInPrestito(((String)s.getAttribute("login")));
				request.setAttribute("libriInPrestito", lbv);
				if(((String)s.getAttribute("level")).equals("administrator"))
				{
					Vector<Libro> ret=null;
					ret=bean.getPrenotazioni();
					request.setAttribute("prenotazioni", ret);
				}
				else request.setAttribute("prenotazioni", null);
				request.setAttribute("level", (String)s.getAttribute("level"));
				rd0.forward(request, response);
			}
		}
		else
		{
			request.setAttribute("result", "Errore: un utente non loggato non può restiruire un libro");
			rdErr.forward(request,response);
		}
	}
	
	protected void prenotaLibro(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		RequestDispatcher rdErr = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
		RequestDispatcher rd0 = getServletContext().getRequestDispatcher("/WEB-INF/cerca.jsp");
		HttpSession s = request.getSession();
		if(s.getAttribute("login")!=null && !s.getAttribute("login").equals("anonymous"))
		{
			if(bean.prenotaLibro(((String)s.getAttribute("login")), request.getParameter("ISBN"),new Integer(request.getParameter("numcopie"))))
			{
				Vector <Libro> lbv = bean.getCatalogo();
				request.setAttribute("tabella", lbv);
				rd0.forward(request, response);
			}
			else
			{
				request.setAttribute("result", "Errore nel metodo prenotaLibro del controller");
				rdErr.forward(request,response);
			}
		}
		else
		{
			request.setAttribute("result", "Errore: un utente non loggato non può prenotare un libro");
			rdErr.forward(request,response);
		}
			
	}
	
	protected void cercaLibri(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		RequestDispatcher rd0 = getServletContext().getRequestDispatcher("/WEB-INF/cerca.jsp");
		HttpSession s = request.getSession();
		Vector <Libro> lbv = bean.getCatalogo();
		request.setAttribute("tabella", lbv);
		request.setAttribute("level",s.getAttribute("level"));
		rd0.forward(request, response);
	}
	
	protected void modificaLibro(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		RequestDispatcher rd0 = getServletContext().getRequestDispatcher("/WEB-INF/modificalibro.jsp");
		RequestDispatcher rdErr = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
		HttpSession s = request.getSession();
		if(s.getAttribute("login")!=null && !s.getAttribute("login").equals("anonymous"))
		{
			Libro formModificaLibro = bean.searchBookByID(request.getParameter("ISBN"));
			request.setAttribute("formModificaLibro", formModificaLibro);
			request.setAttribute("add","disabled");
			rd0.forward(request,response);
		}
		else
		{
			request.setAttribute("result", "Errore: un admin non loggato non può modificare un libro");
			rdErr.forward(request,response);
		}
	}
	
	protected void addLibro(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		RequestDispatcher rd0 = getServletContext().getRequestDispatcher("/WEB-INF/modificalibro.jsp");
		RequestDispatcher rdErr = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
		HttpSession s = request.getSession();
		if(s.getAttribute("level")!=null && s.getAttribute("level").equals("administrator"))
		{
			Libro lb = new Libro("","","",0,0.0);
			request.setAttribute("formModificaLibro", lb);
			request.setAttribute("add","");
			rd0.forward(request,response);
		}
		else
		{
			request.setAttribute("result", "Errore: un admin non loggato non può modificare un libro");
			rdErr.forward(request,response);
		}
	}
	
	protected void modificaLibroAdmin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		HttpSession s = request.getSession();
		RequestDispatcher rdErr = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
		RequestDispatcher rd0 = getServletContext().getRequestDispatcher("/WEB-INF/cerca.jsp");
		if(s.getAttribute("level")!=null && s.getAttribute("level").equals("administrator"))
		{
			String titolo = request.getParameter("titolo");
			String autore = request.getParameter("autore");
			String prezzo = request.getParameter("prezzo");
			String isbn="";
			String copie = request.getParameter("copie");
			if(request.getParameter("isbn_bck") !=null)
				isbn = request.getParameter("isbn_bck");
			else isbn = request.getParameter("isbn");
				
			if(titolo.equals("") || autore.equals("") ||  prezzo.equals("") ||  isbn.equals("") || copie.equals(""))
			{
				request.setAttribute("result","Uno dei campi inseriti e' vuoto. Impossibile completare la richiesta");
				rdErr.forward(request,response);
			}
			else
			{
				if(!bean.modificaLibroAdmin(titolo, autore, prezzo, isbn, copie ))
				{
					request.setAttribute("result"," Impossibile completare la richiesta");
					rdErr.forward(request,response);
				}
				else
				{
					Vector <Libro> lbv=bean.getCatalogo();
					request.setAttribute("tabella", lbv);
					request.setAttribute("level",(String)s.getAttribute("level"));
					rd0.forward(request,response);
				}
			}
		}
		else
		{
			request.setAttribute("result", "Errore: un admin non loggato non può modificare un libro");
			rdErr.forward(request,response);
		}
	}
	protected void eliminaLibro(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		RequestDispatcher rd0 = getServletContext().getRequestDispatcher("/WEB-INF/cerca.jsp");
		RequestDispatcher rdErr = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
		HttpSession s = request.getSession();
		if(s.getAttribute("level")!=null && s.getAttribute("level").equals("administrator"))
		{
			if(bean.eliminaLibro(request.getParameter("ISBN")))
			{
				Vector <Libro> lbv=bean.getCatalogo();
				request.setAttribute("tabella", lbv);
				request.setAttribute("level",s.getAttribute("level"));
				rd0.forward(request,response);
			}		
			else
			{
				request.setAttribute("result", "Non puoi vedere i tuoi libri perche' non sei loggato");
				rdErr.forward(request,response);
			}
		}
		else
		{
			request.setAttribute("result", "Errore: un admin non loggato non può eliminare un libro");
			rdErr.forward(request,response);
		}
	}
	
	protected void confermaAcquisto(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		RequestDispatcher rd0 = getServletContext().getRequestDispatcher("/WEB-INF/paginaPersonale.jsp");
		RequestDispatcher rdERR = getServletContext().getRequestDispatcher("/WEB-INF/error.jsp");
		HttpSession s = request.getSession();
		if(s.getAttribute("login")!=null && !s.getAttribute("login").equals("anonymous"))
		{
			String usrb = (String) s.getAttribute("login");
			if(bean.confermaAcquisto(usrb))
			{
				if(! ((String)s.getAttribute("login")).equals("anonymous") )
				{
					Vector <Libro> ret= bean.libriInPrestito((String)s.getAttribute("login"));
					request.setAttribute("libriInPrestito", ret);
					if(((String)s.getAttribute("level")).equals("administrator"))
					{
						ret=bean.getPrenotazioni();
						request.setAttribute("prenotazioni", ret);
					}
					else request.setAttribute("prenotazioni", null);
					request.setAttribute("level", (String)s.getAttribute("level"));
					rd0.forward(request,response);
				}
				else
				{
					request.setAttribute("result", "Non puoi vedere i tuoi libri perche' non sei loggato");
					rdERR.forward(request,response);
				}
			}		
			else
			{
				request.setAttribute("result", "Impossibile completare l'operazione");
				rdERR.forward(request,response);
			}
		}
		else
		{
			request.setAttribute("result", "Errore: un utente non loggato non può confermare l'acquisto");
			rdERR.forward(request,response);
		}
	}
	
}
