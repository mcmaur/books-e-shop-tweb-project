package myPackage;

public class Libro
{
	private String titolo;
	private String autore;
	private String isbn;
	private int copie;
	private double prezzo;
	private String ext;
	
	public Libro() {
	}

	public Libro(String titolo, String autore, String isbn, int copie,double prezzo) {
		this.titolo = titolo;
		this.autore = autore;
		this.isbn = isbn;
		this.copie = copie;
		this.prezzo = prezzo;
	}
	
	public String getTitolo() {
		return titolo;
	}
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	public String getAutore() {
		return autore;
	}
	public void setAutore(String autore) {
		this.autore = autore;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public int getCopie() {
		return copie;
	}
	public void setCopie(int copie) {
		this.copie = copie;
	}
	public double getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(double prezzo) {
		this.prezzo = prezzo;
	}

	public String toString()
	{
		return "TITOLO: "+titolo+" AUTORE: "+autore+" ISBN: "+isbn+" COPIE: "+copie+" PREZZO: "+prezzo;
	}
	
	public void setExt(String ext){
		this.ext=ext;
	}
	
	public String getExt(){
		return ext;
	}
	
}
