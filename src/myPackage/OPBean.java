package myPackage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

public class OPBean 
{
	//String url = "jdbc:derby://localhost:1527//Users/emanuelechiavelli/firstdb";
	String url = "jdbc:derby://localhost:1527//home/mauro/Documenti/databaseMauro";
	String user_db = "app";
	String pwd = "app";
	
	public OPBean(){}

	public String getLogin(String userName, String password)
	{
		Connection conn=null;
		Statement st=null;
		ResultSet rs=null;
		try 
		{ 
			DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());
			conn = DriverManager.getConnection(url,user_db,pwd);
			st = conn.createStatement();
			System.out.println("SELECT * FROM UTENTI WHERE NOME='" + userName +"' AND PWD='" + password +"'");
			rs = st.executeQuery("SELECT * FROM UTENTI WHERE NOME='" + userName +"' AND PWD='" + password +"'");
			int count=0;
			String type="";
			while(rs.next()){
				count++;
				type=rs.getString("TIPO");
			}
				
			if(count>0) return type;
		} 
		catch (SQLException e) 
		{
			return "-1";
		}
		finally
		{
			try
			{
				conn.close();
				st.close();
				rs.close();
			}
			catch(SQLException sqle)
			{}
		}
		return "-1";
	}
	
	public boolean confermaAcquisto(String user)
	{
		Connection conn=null;
		Statement st=null;
		try
		{
			DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());
			conn = DriverManager.getConnection(url,user_db,pwd);
			st = conn.createStatement();
			System.out.println("DELETE FROM PRESTITI WHERE NOME='"+ user + "'");
			st.executeUpdate("DELETE FROM PRESTITI WHERE NOME='"+ user + "'");
			return true;
		}
		catch (SQLException e)
		{
			return false;
		}
		finally
		{
			try
			{
				conn.close();
				st.close();
			}
			catch(SQLException sqle)
			{}
		}
	}
	
	public boolean register(String userName, String password,String email)
	{
		Connection conn=null;
		Statement st=null;
		try 
		{
			DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());
			conn = DriverManager.getConnection(url,user_db,pwd);
			st = conn.createStatement();
			st.executeUpdate("INSERT * INTO UTENTI WHERE NOME='" + userName +"' AND PWD='" + password +"' AND EMAIL='"+email+"'");
			return true;
		} 
		catch (SQLException e) 
		{
			return false;
		}
		finally
		{
			try
			{
				conn.close();
				st.close();
			}
			catch(SQLException sqle)
			{}
		}
	}
	
	public Vector <Libro> searchBook (String titolo, String autore,String level)
	{
		Connection conn=null;
		Statement st=null;
		ResultSet rs=null;
		try 
		{
			DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());
			conn = DriverManager.getConnection(url,user_db,pwd);
			st = conn.createStatement();
			rs = st.executeQuery("SELECT * FROM LIBRI WHERE TITOLO LIKE '%"+titolo+"%' AND AUTORE LIKE '%"+autore+"%'");
			Vector <Libro> lbv = new Vector <Libro> ();
			while(rs.next())
			{
				Libro lb = new Libro();
				lb.setAutore(rs.getString("Autore"));
				lb.setTitolo(rs.getString("Titolo"));
				lb.setIsbn(rs.getString("ISBN"));
				lb.setPrezzo(new Double(rs.getString("Prezzo")));
				lb.setCopie(new Integer(rs.getString("Copie")));
				lbv.add(lb);
			}
			return lbv;
		} 
		catch (SQLException e) 
		{
			return null;
		}
		finally
		{
			try
			{
				conn.close();
				st.close();
				rs.close();
			}
			catch(SQLException sqle)
			{}
		}
	}
	
	public Vector <Libro> libriInPrestito(String usrname){
		Connection conn=null;
		Statement st=null;
		ResultSet rs=null;
		Vector <Libro> lbv = new Vector <Libro> ();
		try 
		{
			DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());
			conn = DriverManager.getConnection(url,user_db,pwd);
			st = conn.createStatement();
			rs = st.executeQuery("SELECT LIBRI.AUTORE,LIBRI.TITOLO,LIBRI.ISBN, LIBRI.PREZZO, PRESTITI.COPIE FROM PRESTITI,LIBRI,UTENTI " +
								"WHERE PRESTITI.NOME=UTENTI.NOME AND UTENTI.NOME='"+usrname+"' AND LIBRI.ISBN=PRESTITI.ISBN");
			while(rs.next())
			{
				Libro lb = new Libro();
				lb.setAutore(rs.getString("Autore"));
				lb.setTitolo(rs.getString("Titolo"));
				lb.setIsbn(rs.getString("ISBN"));
				lb.setPrezzo(new Double(rs.getString("Prezzo")));
				if(rs.getString("Copie")!=null) lb.setCopie(new Integer(rs.getString("Copie")));
				lbv.add(lb);
			}			
			return lbv;
		} 
		catch (SQLException e) 
		{
			return null;
		}
		finally
		{
			try
			{
				conn.close();
				st.close();
				rs.close();
			}
			catch(SQLException sqle)
			{}
		}
	}
	
	public boolean restituisciLibro(String usrname, String ISBN)
	{
		Connection conn=null;
		Statement st=null;
		ResultSet rs=null;
		int copie=0;
		try 
		{
			DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());
			conn = DriverManager.getConnection(url,user_db,pwd);
			st = conn.createStatement();
			rs = st.executeQuery("SELECT COPIE FROM PRESTITI WHERE PRESTITI.NOME='" + usrname + "' AND PRESTITI.ISBN='" + ISBN + "'");
			while(rs.next())
			{
				copie=rs.getInt("COPIE");
			}
			
			if(copie==1)
				st.executeUpdate("DELETE FROM PRESTITI WHERE PRESTITI.NOME='" + usrname + "' AND PRESTITI.ISBN='" + ISBN + "'");
			else{
				System.out.println("UPDATE LIBRI SET COPIE=COPIE+1 WHERE ISBN='" + ISBN + "'");
				st.executeUpdate("UPDATE LIBRI SET COPIE=COPIE+1 WHERE ISBN='" + ISBN + "'");
				st.executeUpdate("UPDATE PRESTITI SET COPIE=COPIE-1 WHERE PRESTITI.NOME='" + usrname + "' AND PRESTITI.ISBN='" + ISBN + "'");
				System.out.println("<OPBEAN:190> Dopo di Update");
			}
				
			return true;
		}
		catch (SQLException e)
		{
			return false;
		}
		finally
		{
			try
			{
				conn.close();
				st.close();
				rs.close();
			}
			catch(SQLException sqle)
			{}
		}
	}
	
	public Vector <Libro> getCatalogo()
	{
		Connection conn=null;
		Statement st=null;
		ResultSet rs=null;
		Vector <Libro> lbv = new Vector <Libro> ();
		try 
		{
			DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());
			conn = DriverManager.getConnection(url,user_db,pwd);
			st = conn.createStatement();
			rs=st.executeQuery("SELECT * FROM LIBRI");
			//creo tabella
			while(rs.next())
			{
				Libro lb = new Libro();
				lb.setAutore(rs.getString("Autore"));
				lb.setTitolo(rs.getString("Titolo"));
				lb.setIsbn(rs.getString("ISBN"));
				lb.setPrezzo(new Double(rs.getString("Prezzo")));
				if(rs.getString("Copie")!=null) lb.setCopie(new Integer(rs.getString("Copie")));
				lbv.add(lb);
			}
			return lbv;
		}
		catch (SQLException e)
		{
			return null;
		}
		finally
		{
			try
			{
				conn.close();
				st.close();
				rs.close();
			}
			catch(SQLException sqle)
			{}
			
		}
	}

	public boolean prenotaLibro(String usname,String ISBN,int numero_copie)
	{
		Connection conn=null;
		Statement st=null;
		ResultSet rs=null;
		try 
		{
			int codice=0,copie=0;
			DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());
			conn = DriverManager.getConnection(url,user_db,pwd);
			st = conn.createStatement();
			rs=st.executeQuery("SELECT MAX(CODICE) AS COD FROM PRESTITI");
			while(rs.next())
			{
				codice=Integer.parseInt(rs.getString("cod"));
			}
			codice++;
			
			rs=st.executeQuery("SELECT COPIE AS C FROM LIBRI WHERE ISBN='" + ISBN + "'");
			while(rs.next())
			{
				copie=Integer.parseInt(rs.getString("C"));
			}
			if(copie>0){
				st.executeUpdate("INSERT INTO PRESTITI (NOME,ISBN,COPIE,CODICE) VALUES ('" + usname + "','" + ISBN + "',"+ numero_copie+","+ codice + ")");
				st.executeUpdate("UPDATE LIBRI SET COPIE=COPIE-"+numero_copie+" WHERE ISBN='" + ISBN + "'");
				return true;
			}
			else return false;
		}
		catch (SQLException e)
		{
			return false;
		}
		finally
		{
			try
			{
				conn.close();
				st.close();
			}
			catch(SQLException sqle)
			{}
		}
	}
	
	public Libro searchBookByID (String ISBN)
	{
		Connection conn=null;
		Statement st=null;
		ResultSet rs=null;
		Libro lb = new Libro();
		try
		{
			DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());
			conn = DriverManager.getConnection(url,user_db,pwd);
			st = conn.createStatement();
			rs = st.executeQuery("SELECT * FROM LIBRI WHERE ISBN='"+ISBN+"'");
			while(rs.next())
			{	
				lb.setAutore(rs.getString("Autore"));
				lb.setTitolo(rs.getString("Titolo"));
				lb.setIsbn(rs.getString("ISBN"));
				lb.setPrezzo(new Double(rs.getString("Prezzo")));
				lb.setCopie(new Integer(rs.getString("Copie")));
			}
			return lb;
		}
		catch (SQLException e)
		{
			return null;
		}
		finally
		{
			try
			{
				conn.close();
				st.close();
				rs.close();
			}
			catch(SQLException sqle)
			{}
		}
	}
	
	public boolean modificaLibroAdmin(String titolo,String autore,String prezzo,String isbn,String copie)
	{
		Connection conn=null;
		Statement st=null;
		try
		{
			DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());
			conn = DriverManager.getConnection(url,user_db,pwd);
			st = conn.createStatement();
			st.executeUpdate("DELETE FROM LIBRI WHERE ISBN='"+isbn+"'");
			st.executeUpdate("DELETE FROM PRESTITI WHERE ISBN='"+isbn+"'");
			st.executeUpdate("INSERT INTO LIBRI (TITOLO,AUTORE,ISBN,PREZZO,COPIE) VALUES('"+titolo+"','"+autore+"','"+isbn+"',"+prezzo+","+copie+")");
			return true;
		}
		catch (SQLException e)
		{
			return false;
		}
		finally
		{
			try
			{
				conn.close();
				st.close();
			}
			catch(SQLException sqle)
			{}
		}
	}
	
	public boolean eliminaLibro(String ISBN){
			Connection conn=null;
			Statement st=null;
			try
			{
				DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());
				conn = DriverManager.getConnection(url,user_db,pwd);
				st = conn.createStatement();
				st.executeUpdate("DELETE FROM LIBRI WHERE ISBN='"+ISBN+"'");
				return true;
			}
			catch (SQLException e)
			{
				return false;
			}
			finally
			{
				try
				{
					conn.close();
					st.close();
				}
				catch(SQLException sqle)
				{}
			}
		}
	
	public Vector<Libro> getPrenotazioni(){
		Connection conn=null;
		Statement st=null;
		ResultSet rs=null;
		Vector<Libro> libvector = new Vector<Libro>();
		try
		{
			DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());
			conn = DriverManager.getConnection(url,user_db,pwd);
			st = conn.createStatement();
			rs=st.executeQuery("SELECT PRESTITI.NOME,LIBRI.TITOLO,LIBRI.AUTORE,PRESTITI.ISBN FROM PRESTITI,LIBRI WHERE PRESTITI.ISBN = LIBRI.ISBN");
			
			while(rs.next())
			{	
				Libro lb = new Libro();
				lb.setAutore(rs.getString("Autore"));
				lb.setTitolo(rs.getString("Titolo"));
				lb.setIsbn(rs.getString("ISBN"));
				lb.setExt(rs.getString("Nome"));
				libvector.add(lb);
			}
			return libvector;
		}
		catch (SQLException e)
		{
			return null;
		}
		finally
		{
			try
			{
				conn.close();
				st.close();
			}
			catch(SQLException sqle)
			{}
		}
	}
	
}
